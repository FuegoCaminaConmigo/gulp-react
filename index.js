'use strict';

var gutil = require('gulp-util');
var through = require('through2');
var applySourceMap = require('vinyl-sourcemaps-apply');
var react = require('react-tools');

module.exports = function (opts) {
	opts = opts || {};

	return through.obj(function (file, enc, cb) {
		if (file.isNull()) {
			cb(null, file);
			return;
		}

		if (file.isStream()) {
			cb(new gutil.PluginError('gulp-react', 'Streaming not supported'));
			return;
		}

		if (file.sourceMap) {
	      opts.sourceMap = true;
	    }

		try {
			var result = react.transformWithDetails(file.contents.toString(), opts);
			file.contents = new Buffer(result.code);
			if (file.sourceMap && result.sourceMap) {
				result.sourceMap.sources = [file.relative];
				applySourceMap(file, result.sourceMap);
			}
			file.path = gutil.replaceExtension(file.path, '.js');
			this.push(file);
		} catch (err) {
			this.emit('error', new gutil.PluginError('gulp-react', err, {
				fileName: file.path
			}));
		}

		cb();
	});
};
